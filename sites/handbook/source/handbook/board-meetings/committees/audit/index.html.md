---
layout: handbook-page-toc
title: "Audit Committee"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Audit Committee Composition

- **Chairperson:** Karen Blasing
- **Members:** Sunny Bedi, David Hornik
- **Management DRI:** Chief Financial Officer

## Audit Committee Charter

**THE AMENDED AND RESTATED CHARTER WAS APPROVED BY THE BOARD ON 2021-09-14**

Please click here to read the [Audit Committee Charter](https://ir.gitlab.com/static-files/f8d37e2d-d022-4194-996f-6d194cd02173)

## Audit Committee Agenda Planner

The below amended Audit Committee `Earnings` and `Compliance and Special Topics` Meeting Calendar was approved by the Audit Committee on 2021-12-15.

### Audit Committee Earnings Meeting Calendar <br>

#### Accounting and Reporting

|#|Topics                                                              | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:|:-------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---:  |
| 1 |Financial Summary                       |   X   |    X   |   X   |   X  |       |
| 2 |Statement of Operations - GAAP          |   X   |    X   |   X   |   X  |       |
| 3 |Review of Financial Statements          |   X   |    X   |   X   |   X  |       |
| 4 |Balance Sheet                           |   X   |    X   |   X   |   X  |       |
| 5 |Statement of Cash Flow                  |   X   |    X   |   X   |   X  |       |
| 6 |GAAP to Non-GAAP Reconciliation         |   X   |    X   |   X   |   X  |       |
| 7 |Review of Financial Statement - Summary of Key Accounting Matters     |   X   |    X   |   X   |   X  |       |
| 8 |Audit Update                            |   X   |    X   |   X   |   X  |       |
| 9 |Other Topics                                        |     |    |     |     |  X  |


#### Tax

|#|Topics                                                              | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:|:-------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---:  |
| 1 |Summary of Key Tax Metrics                      |   X   |    X   |   X   |   X |       |
| 2 |Other Topics                                        |     |    |     |     |  X  |


#### Disclosure Committee

|#|Topics                                                                  | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :----------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Disclosure Committee Report          |   X   |    X   |   X   |   X |       |
| 2 |Other Topics                                        |     |    |     |     |  X  |


#### 10Q /10K

|#| Topics                                                                             | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :------------------------------------------------------------------------ | :---: | :---: | :---: | :---: |:---: |
| 1 | 10Q /10K Summary and Draft 10Q /10K        |  X  |   X  |   X   |   X   |      |
| 2 |Other Topics                                        |     |    |     |     |  X  |


#### Internal Audit

|#| Topics                           | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :------------------------------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Internal Audit Update                       |      |       |      |    |   X   |
| 2 |Other Topics                                        |     |    |     |     |  X  |


#### Earnings Release and Guidance

|#| Topics                                                                        | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :-------------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Press Release                 |  X  |   X  |   X   |   X   |      |
| 2 | Prepared Remarks              |  X  |   X  |   X   |   X   |      |
| 3 | Investor Presentation         |  X  |   X  |   X   |   X   |      |
| 4 | Guidance Review               |  X  |   X  |   X   |   X   |      |
| 5 |Other Topics                                        |     |    |     |     |  X  |


#### KPMG

|#| Topics                                     | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Quarterly Review Update                  |   X   |   X   |   X   |   X   |      |
| 2 |Other Topics                                        |     |    |     |     |  X  |


#### Closed session 

|#| Topics                                     | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Chief Legal Officer, Chief Finance Officer and kPMG       |  X  |   X  |   X   |   X   |      |


<br>

### Audit Committee Compliance and Special Topics Meeting Calendar <BR>

#### Management, Accounting and Reporting

|#|Topics                                 | Frequency | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:|:-------------------------------------------------------------- | :---: | :---: | :---: | :---: | :---: |:---:  |
| 1 |New Accounting Pronouncements                         |Annual |  X  |    |     |     |     |
| 2 |International Reporting                               |Annual |  X  |    |     |     |     |
| 3 |Treasury                                              |Annual |  X  |    |  X  |     |     |
| 4 |Tax/Economic Ownership Structure                      |Annual |     |    |     |  X  |     |
| 5 |Effective Tax Rate/Cash Taxes/Tax Attributes (NOLs and Credits)                                                |Quarterly |  X  | X  |  X  |  X  |     |
| 6 |Stat Audit Update                                     |Annual |     |    |  X  |     |     |
| 7 |Audit Committee Calendar Review                       |Annual |     |    |     |  X  |     |
| 8 |Organization Overviews - Accounting, Finance, Tax, FP&A, Investor Relations                                             |Semi Annual |     | X  |     |  X  |     |
| 9 |Insurance Coverage Update                          |As needed |     |    |     |     |  X  |
|10 |Stock Transactions                                 |As needed |     |    |     |     |  X  |
|11 |Tax Audits and Assessments/Closing Agreements      |As needed |     |    |     |     |  X  |
|12 |Attrition                                          |As needed |     |    |     |     |  X  |
|13 |Investment Program and Health Check              |Semi Annual |     | X  |     |  X  |     |
|14 |Close Process Assessment & SEC                   |Semi Annual |     | X  |     |  X  |     |
|15 |Other Topics                                         |As needed |     |    |     |     |  X  |


#### People Division

|#|Topics                                                        | Frequency | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:|:-------------------------------------------------------------- | :---: | :---: | :---: | :---: | :---: |:---: |
| 1 | Global staffing update                                           | Annual|       |   X   |       |       |      |
| 2 | Team assessment, shared service plan and succession planning     | Annual|       |   X   |       |       |      |
| 3 | EEO audits                                                       | Annual|       |   X   |       |       |      |
| 4 | Payroll, Compensation and hiring                                 | Annual|       |   X   |       |       |      |
| 5 | Organization Overview                                            | Annual|       |   X   |       |       |      |
| 6 |Other Topics                                         |As needed |     |    |     |     |  X  |


#### Legal, Risk and Compliance

|#|Topics                                 | Frequency | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:|:-------------------------------------------------------------- | :---: | :---: | :---: | :---: | :---: |:---:  |
| 1 |Compliance to Business Conduct (including hotline complaints and code of conduct violations)     |As needed |     |    |     |     |  X  |
| 2 |Code of Conduct, Related Party Transactions and other policy reviews   |As needed |     |     |     |     |  X  |
| 3 |Legal Risk Assessment updates           |As needed |     |     |     |     |  X  |
| 4 |Regulatory Compliance                   |As needed |     |     |     |     |  X  |
| 5 |Privacy & Product Compliance / Employment  |Annual |     |  X  |     |     |  X  |
| 6 |Committee Annual Assessment                |Annual |  X  |     |     |     |  X  |
| 7 |Related Party Transactions              |As needed |     |     |     |     |  X  |
| 8 |Litigation                              |As needed |     |     |     |     |  X  |
| 9 |Review of Audit Committee Charter          |Annual |  X  |     |     |     |  X  |
|10 |Organization Overview                      |Annual |     |  X  |     |     |  X  |
|11 |Approval of Minutes                     |Quarterly |  X  |  X  |  X  |  X  |     |
|12 |Other Topics                                         |As needed |     |    |     |     |  X  |


#### System

|#|Topics                                 | Frequency | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:|:-------------------------------------------------------------- | :---: | :---: | :---: | :---: | :---: |:---:  |
| 1 | Information Technology System Updates (accomplishments, focus and improvements)  |Semi Annual |      | X  |     |  X  |     |
| 2 | State of Systems and Scalability      |Semi Annual |  X   |    |  X  |     |     |
| 3 | Organization Overview                      |Annual |  X   |    |     |     |     |
| 4 |Other Topics                                         |As needed |     |    |     |     |  X  |


#### Security Compliance

|#|Topics                                 | Frequency | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:|:-------------------------------------------------------------- | :---: | :---: | :---: | :---: | :---: |:---:  |
| 1 | Security Operational Risk Assessment Results         |Annual |      |  X  |     |     |     |
| 2 | Security Update                                 |Semi Annual |      |  X  |     |  X  |     |
| 3 | Finance Application System Reviews (ITGC)       |Semi Annual |  X   |     |  X  |     |     |
| 4 | Organization Overview                                |Annual |      |     |     |  X  |     |
| 5 |Other Topics                                         |As needed |     |    |     |     |  X  |


#### Internal Audit

|#|Topics                                 | Frequency | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:|:-------------------------------------------------------------- | :---: | :---: | :---: | :---: | :---: |:---:  |
| 1 | Internal Audit Annual Plan             |Annual |  X   |     |     |     |     |
| 2 | Fraud Risk Assessment                  |Annual |      |  X  |     |     |     |
| 3 | Annual Assessment of Internal Audit    |Annual |      |     |  X  |     |     |
| 4 | Internal Audit Charter Review       |As needed |   X  |     |     |     |     |
| 5 | Organization Overview               |As needed |      |  X  |     |     |     |
| 6 | SOX Program Update                |Semi Annual |  X   |     |  X  |     |     |  
| 7 |Other Topics                                    |As needed |     |    |     |     |  X  |


#### External Audit

|#|Topics                                 | Frequency | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:|:-------------------------------------------------------------- | :---: | :---: | :---: | :---: | :---: |:---:  |
| 1 | Global Audit Plan and Fees/Appoint External Auditor   |Annual |     |     |     |  X  |     |
| 2 | Annual Assessment of Audit Firm, Engagement Team and Lead Audit Partner  |Annual |     |     |  X   |     |     |
| 3 | Independence Review                        |Annual |     |     |     |  X   |     |
| 4 | Quarterly and Annual Audit Results and required communications, as applicable  |Quarterly |  X   |  X   |  X   |   X  |    |
| 5 | Other Topics                                         |As needed |     |    |     |     |  X  |


#### Closed session 

|#|Topics                                 | Frequency | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:|:-------------------------------------------------------------- | :---: | :---: | :---: | :---: | :---: |:---:  |
| 1 | Executive Session with invitees    |As needed |       |       |       |       |   X  |
| 2 | Chief Legal Officer                |Quarterly |   X   |   X   |   X   |   X   |      |
| 3 | Chief Finance Officer              |Quarterly |   X   |   X   |   X   |   X   |      |
| 4 | Approvals                          |Quarterly |   X   |   X   |   X   |   X   |      |
| 5 | External Auditor                   |Quarterly |   X   |   X   |   X   |   X   |      |

<br>

## Audit Committee Meeting Deck Preparation Guidelines

**Responsibility: [Chief Financial Officer](https://about.gitlab.com/company/team/#brobins) / [Principal Accounting Officer](https://about.gitlab.com/company/team/#daleb04)**

1. All the audit committee decks should be saved in [google drive](https://drive.google.com/drive/folders/1nqK9DZC84qbV6b6rKwjt0NgpVjip1WnW).
1. Refer to GitLab [Board Calendar](https://docs.google.com/spreadsheets/d/1GW59GiT0MLXEgMxy2bN0ZVcbMs_wssOkP2c59f19YTk/edit#gid=519993910) and identify Audit Committee Meeting Date.
1. Copy the [Format](https://docs.google.com/presentation/d/15k15TYvTGkxZizBds1geY3lTlIq1nfU9ofwwynoY9dM/edit#slide=id.g6478e21bce_0_0) of the Audit Committee meeting deck and rename the deck as **Audit Committee Meeting Month,MM, Day**
1. | Update the agenda slide of the deck by                                                                                                                                 |
    | --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
    | a) Referring to the [master calendar](https://docs.google.com/spreadsheets/d/1C7JOGCtVJYgyjorHNch3oQT8tLtyYL9u4s88arm6d58/edit#gid=1897678424) -> Audit Committee calendar tab FY22 |
    | b) [Handbook](https://about.gitlab.com/handbook/board-meetings/committees/audit/#audit-committee-agenda-planner) for Audit Committee meeting agenda items                                                      |
    | c) Audit Committee meeting [notes](https://docs.google.com/document/d/1D6wpUqqx9y_AcMyJr2XmdGsaauvplkLFmgbjUh9ESX0/edit)                                              |
1. Set up a meeting with Principal Accounting Officer to review and update all the agenda items.
1. Once the agenda is finalized, create slides for each agenda item and assign to respective DRI’s with a due date for completion; at least  10 days before the meeting.
1. Tag all the DRI’s on **#Audit Committee** slack channel, linking the Deck and communicating the due date for completion of the deck.
1. Follow up with all the DRI's at least a week before the due date.
1. Once respective DRI's update their slides, review the format, update slides to ensure format is consistent across all the slides.
1. Set up a call with the Principal Accounting officer on the due date of the deck/ next immediate day to review the deck. Make necessary changes based on the review.
1. Set up a call for CFO's review along with Principal Accounting Officer. Make necessary changes to the deck based on CFO's review.
1. The Sr. EBA to the CFO will upload the final deck to Boardvantage at least a week prior to the meeting. The CFO will notify the Committee members of such via email and copying relevant DRIs.  
1. Update Audit Committee [meeting notes](https://docs.google.com/document/d/1D6wpUqqx9y_AcMyJr2XmdGsaauvplkLFmgbjUh9ESX0/edit#heading=h.nu90jml2xhx2) with agenda items and DRI’s.
1. On the meeting day
    - Make note of the follow-up items, add them to the agenda under ask from the Audit Committee section in in the [AC calendar](https://docs.google.com/spreadsheets/d/1C7JOGCtVJYgyjorHNch3oQT8tLtyYL9u4s88arm6d58/edit#gid=1897678424) along with the due date.
    - Schedule a call after the day of the Audit Committee meeting with the Principal Accounting Officer to review the agenda for the next meeting.
