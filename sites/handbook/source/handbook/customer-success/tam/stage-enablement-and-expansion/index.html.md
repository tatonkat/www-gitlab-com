---
layout: handbook-page-toc
title: "Stage Enablement and Expansion"
---

## Stage Enablement & Stage Expansion: The Two Stage Adoption Motions

The TAM team's primary focus is to align with a customer's desired business outcomes, enable the customer in their current use case, and expand the customer's use case into new stages. We accomplish this through stage adoption, which has two critical motions: enablement and expansion.

### Why does stage enablement & expansion matter?

GitLab’s primary point of differentiation is our [single application](/handbook/product/single-application/) approach. As we continue to drive value in any given stage or category, our first instinct should be to connect that feature or product experience to other parts of the GitLab product. These cross-stage connections will drive differentiated customer value, and will be impossible for point product competitors to imitate.  

Our [mission as a customer success team](/handbook/customer-success/#mission-statement) is to create value for our customers. As a customer adopts additional [DevOps stages](/stages-devops-lifecycle/) with GitLab, they see an increased return-on-investment (ROI). They receive this increased ROI by [increasing operational efficiencies, delivering better products faster, and reducing security and compliance risk](/handbook/sales/command-of-the-message/#customer-value-drivers).

### What is the difference between stage enablement & stage expansion?

For the TAM team in driving stage enablement, there are two critical motions:
1. Stage Enablement: Enabling on the stages a customer expresses readiness to expand into (creating quick time to value, overcoming technical roadblocks, and ensuring stickiness). The TAM will work with the customer to provide guidance on adopting stages to maximize value attribution and align adoption to the customer's desired positive business outcomes. Enablement can occur via TAM-led webinars, digital content, and/or professional services. This is a **customer-driven** adoption play.
1. Stage Expansion: Driving expansion into new stages as a part of an account planning motion, in-line with the SAL or AE, as a means of ensuring the customer receives increasing ROI and creating happy customers that grow. This motion starts with a discovery process to open the door to a more in-depth discovery, with demos and [workshops](/handbook/customer-success/#customer-workshops) focused on value-positioning that lead to the 'yes'. The latter part of an expansion play is the enablement of the stage, as articulated above. This is a **TAM-driven** adoption play.

### Where do I track the stage enablement and expansion?

1. Both of these CTAs are to be opened in the [ROI success plan](/handbook/customer-success/tam/success-plans/#roi-success-plan). Select the appropriate playbook from the playbook section at the CTA level before saving. This then opens the suggested steps as tasks associated with this CTA.

**Note:** To ensure that your Stage Enablement or Stage Expansion CTA is included in reporting, please make sure to attach a playbook to the CTA.


### When do I open a CTA/playbook?

1. Stage Enablement: open the playbook when the customer is ready to begin the process of adopting the stage (vs when they say they intend to at a later time).
1. Stage Expansion: open the playbook when you plan to **start** the motion of exploring this expansion with the customer through the discovery process. This timing is incredibly key as capturing the 'no' reasons is very impactful for articulating through data the roadblocks to expansion we see and building scaleable plays for overcoming these roadblocks.  We expect to see more than 50% of our playbooks here being closed-no success for reasons of timing, internal challenges or otherwise!  The goal here is to continue exploring expansion with the customer as it pertains to furthering their business value with GitLab and collecting common reasons for no-success in order to establish more plays to help us overcome objections. 


### Key TAM Strategies:

1. Improve onboarding experience
1. Reduce customer's time to value
1. Provide digital enablement for customer to use
1. Contribute knowledge to GitLab docs and blogs
1. Hold stage expansion and enablement workshops and webinars
1. Ask effective trap-setting questions for plays based on proven paths to adoption
1. Help us establish metrics on time-per-stage for adoption and insights on successes and non-successes
